use clap::Parser;
use std::io::{self, BufRead};
use std::process;
use std::str;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// MAC address format
    #[clap(short, long, value_parser, value_name = ": or - or .", default_value_t = String::from(":"))]
    format: String,
    /// MAC Address. "-" for reading from stdin
    #[clap(value_parser)]
    macaddr: String,
}
fn normalize_mac(mac: &str, separator: &str) -> String {
    let cmac = mac.replace(":", "").replace("-", "").replace(".", "");
    match separator {
        ":" | "-" => {
            let subs = cmac
                .as_bytes()
                .chunks(2)
                .map(|buf| unsafe { str::from_utf8_unchecked(buf) })
                .collect::<Vec<&str>>();
            subs.join(separator)
        }
        "." => {
            let subs = cmac
                .as_bytes()
                .chunks(4)
                .map(|buf| unsafe { str::from_utf8_unchecked(buf) })
                .collect::<Vec<&str>>();
            subs.join(separator)
        }
        _ => {
            println!("Unsupported MAC format");
            process::exit(-1)
        }
    }
}

fn main() {
    //let args: Vec<String> = env::args().collect();
    let cli = Cli::parse();
    //    if args.len() < 2 {
    //       println!("Podaj adres MAC jako argument");
    //      process::exit(-1);
    // }
    //let data = &args[1];
    if cli.macaddr != "-" {
        println!("{}", normalize_mac(&cli.macaddr, &cli.format));
    } else {
        let stdin = io::stdin();
        let line = stdin.lock().lines().next().unwrap().unwrap();
        println!("{}", normalize_mac(&line, &cli.format));
    }
}
